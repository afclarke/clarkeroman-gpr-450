/*
	Copyright 2011-2020 Daniel S. Buckstein

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

/*
	animal3D SDK: Minimal 3D Animation Framework
	By Daniel S. Buckstein
	
	a3_HierarchyStateBlend.h
	Hierarchy blend operations.
*/

#ifndef __ANIMAL3D_HIERARCHYSTATEBLEND_H
#define __ANIMAL3D_HIERARCHYSTATEBLEND_H


#include "a3_HierarchyState.h"

#include "a3_Kinematics.h"


#ifdef __cplusplus
extern "C"
{
#else	// !__cplusplus
typedef struct a3_SpatialPoseBlendNode a3_SpatialPoseBlendNode;
typedef struct a3_SpatialPoseBlendTree a3_SpatialPoseBlendTree;
#endif	// __cplusplus
	
//-----------------------------------------------------------------------------

// execution template (custom name mangling, just use overrides in c++)
typedef void (*a3_SpatialPoseExec)(a3_SpatialPoseBlendNode const* blendNode);
void a3_SpatialPoseExec0C0I(a3_SpatialPoseBlendNode const* blendNode);
void a3_SpatialPoseExec2C1I(a3_SpatialPoseBlendNode const* blendNode);


void a3_SpatialPoseTreeExec(a3_SpatialPoseBlendTree const* blendTree);

	
// operation template for any spatial pose operation
//typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose const**, a3real const**);
//typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose* pose_out);
//typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose* pose_out, a3_SpatialPose const* pose_in); // copy
//typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose* pose_out, a3_SpatialPose* pose_A, a3_SpatialPose* pose_B); // concat
//typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose* pose_out, a3_SpatialPose* pose_A, a3_SpatialPose* pose_B, a3real u); // lerp
typedef a3_SpatialPose* (*a3_SpatialPoseBlendOp)(a3_SpatialPose* pose_out, ...);

//-----------------------------------------------------------------------------
	
// complete representation of a blend operation
struct a3_SpatialPoseBlendNode
{
	//execution invocation
	a3_SpatialPoseExec exec;
	
	// the op itself
	a3_SpatialPoseBlendOp op;

	// output pose
	a3_SpatialPose* pose_out;

	// control poses
	a3_SpatialPose const* pose_ctrl[16]; // max 16
	
	// parameters
	// using pointers for flexibility, so that the node can always have an updated value by pointing to something else
	a3real const* u[8]; // max 8

	// optional: how many of the above
	a3ui16 ctrlCount, paramCount;
};

struct a3_SpatialPoseBlendTree
{
	a3_Hierarchy const* blendTreeDescriptor;
	
	a3_SpatialPoseBlendNode* nodes;
};
	
//-----------------------------------------------------------------------------

// pointer-based reset/identity operation for single spatial pose
a3_SpatialPose* a3spatialPoseOpIdentity(a3_SpatialPose* pose_out);

// pointer-based LERP operation for single spatial pose
a3_SpatialPose* a3spatialPoseOpLERP(a3_SpatialPose* pose_out, a3_SpatialPose const* pose0, a3_SpatialPose const* pose1, a3real const u);


//-----------------------------------------------------------------------------

// data-based reset/identity
a3_SpatialPose a3spatialPoseDOpIdentity();

// data-based LERP
a3_SpatialPose a3spatialPoseDOpLERP(a3_SpatialPose const pose0, a3_SpatialPose const pose1, a3real const u);


//-----------------------------------------------------------------------------

// pointer-based reset/identity operation for hierarchical pose
a3_HierarchyPose* a3hierarchyPoseOpIdentity(a3_HierarchyPose* pose_out);

// pointer-based LERP operation for hierarchical pose
a3_HierarchyPose* a3hierarchyPoseOpLERP(a3_HierarchyPose* pose_out, a3_HierarchyPose const* pose0, a3_HierarchyPose const* pose1, a3real const u);


//-----------------------------------------------------------------------------


#ifdef __cplusplus
}
#endif	// __cplusplus


#include "_inl/a3_HierarchyStateBlend.inl"


#endif	// !__ANIMAL3D_HIERARCHYSTATEBLEND_H