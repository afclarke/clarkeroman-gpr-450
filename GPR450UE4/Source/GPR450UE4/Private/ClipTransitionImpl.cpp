//

#include "ClipTransitionImpl.h"
#include "ClipPool.h"

TMap<ETransitionAction, TFunction<void(FClipState& clipState,
	FKeyframeState& keyframeState)>> ClipTransitionImpl::Transitions =
{
	{ ETransitionAction::Forward, ForwardTransition },
	{ ETransitionAction::ForwardPause, ForwardPauseTransition },
	{ ETransitionAction::Reverse, ReverseTransition },
	{ ETransitionAction::ReversePause, ReversePauseTransition },
	{ ETransitionAction::ForwardSkip, ForwardSkipTransition },
	{ ETransitionAction::ForwardSkipPause, ForwardSkipPauseTransition },
	{ ETransitionAction::ReverseSkip, ReverseSkipTransition },
	{ ETransitionAction::ReverseSkipPause, ReverseSkipPauseTransition }
};

void ClipTransitionImpl::Transition(FClipState& clipState, FKeyframeState& keyframeState)
{
	const FClipTransition transition = clipState.CurClip->GetClipTransition();

	// Pause transition (does not set clip before transition)
	if (transition.mTransitionAction == ETransitionAction::Pause)
	{
		PauseTransition(clipState, keyframeState);
		return;
	}

	// Other transitions
	// set clip to target clip
	SetClip(clipState, transition.mTargetClipName);
	// execute transition action
	Transitions[transition.mTransitionAction](clipState, keyframeState);
}

void ClipTransitionImpl::SetKeyframe(const FClipState& clipState, FKeyframeState& keyframeState,
	int32 keyframeIndex)
{
	KeyframePool* keyframePool = KeyframePool::GetInstance();

	Clip* curClip = clipState.CurClip;
	int32 firstKeyframeIndex = curClip->GetFirstKeyframeIndex();
	int32 lastKeyframeIndex = curClip->GetLastKeyframeIndex();
	int32 clipNumKeyframes =
		curClip->GetLastKeyframeIndex() - firstKeyframeIndex + 1;

	// get new keyframe (looping)
	keyframeState.Keyframe0Index = ((keyframeIndex - firstKeyframeIndex) % clipNumKeyframes) + firstKeyframeIndex;
	// next keyframe index
	if (clipState.PlaybackMode == EPlaybackMode::Forward)
	{
		keyframeState.KeyframePIndex = (((keyframeIndex - firstKeyframeIndex) - 1 + clipNumKeyframes) % clipNumKeyframes) + firstKeyframeIndex;
		keyframeState.Keyframe1Index = (((keyframeIndex - firstKeyframeIndex) + 1) % clipNumKeyframes) + firstKeyframeIndex;
		keyframeState.KeyframeNIndex = (((keyframeIndex - firstKeyframeIndex) + 2) % clipNumKeyframes) + firstKeyframeIndex;
	}
	else
	{
		keyframeState.KeyframePIndex = (((keyframeIndex - firstKeyframeIndex) + 1) % clipNumKeyframes) + firstKeyframeIndex;
		keyframeState.Keyframe1Index = (((keyframeIndex - firstKeyframeIndex) - 1 + clipNumKeyframes) % clipNumKeyframes) + firstKeyframeIndex;
		keyframeState.KeyframeNIndex = (((keyframeIndex - firstKeyframeIndex) - 2 + clipNumKeyframes) % clipNumKeyframes) + firstKeyframeIndex;
	}

	Keyframe* curKeyframe = keyframePool->GetKeyframe(keyframeState.Keyframe0Index);
	// cache duration
	keyframeState.KeyframeDuration = curKeyframe->GetDuration();
	keyframeState.KeyframeDurationInv = curKeyframe->GetInverseDuration();
}

void ClipTransitionImpl::SetClip(FClipState& clipState, const FString& targetClipName)
{
	ClipPool* clipPool = ClipPool::GetInstance();
	int32 targetClipIndex = clipPool->GetClipIndex(targetClipName);

	check(targetClipIndex >= 0);

	clipState.CurClipIndex = targetClipIndex;
	clipState.CurClip = clipPool->GetClip(clipState.CurClipIndex);
	clipState.ClipDuration = clipState.CurClip->GetDuration();
	clipState.ClipDurationInv = clipState.CurClip->GetInverseDuration();
}

void ClipTransitionImpl::PauseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// if forward, pause at forward terminus / clip end
	if (clipState.PlaybackMode == EPlaybackMode::Forward)
	{
		keyframeState.KeyframeTime = keyframeState.KeyframeDuration;
		keyframeState.KeyframeParameter = 1;
		clipState.ClipTime = clipState.CurClip->GetDuration();
		clipState.ClipParameter = 1;
	}
	// else Reverse (would never transition while in paused mode)
	// pause at reverse terminus / clip beginning
	else
	{
		keyframeState.KeyframeTime = 0;
		keyframeState.KeyframeParameter = 0;
		clipState.ClipTime = 0;
		clipState.ClipParameter = 0;
	}

	// pause
	clipState.PlaybackMode = EPlaybackMode::Pause;
}

void ClipTransitionImpl::ForwardTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// continue from first keyframe
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetFirstKeyframeIndex());
}

void ClipTransitionImpl::ForwardPauseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// pause at first frame of next clip
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetFirstKeyframeIndex());
	// reset time to begin terminus
	keyframeState.KeyframeTime = 0;
	keyframeState.KeyframeParameter = 0;
	clipState.ClipTime = 0;
	clipState.ClipParameter = 0;

	// pause
	clipState.PlaybackMode = EPlaybackMode::Pause;
}

void ClipTransitionImpl::ReverseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// continue from last keyframe
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetLastKeyframeIndex());
}

void ClipTransitionImpl::ReversePauseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// pause at last frame of next clip
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetLastKeyframeIndex());
	// reset time to begin terminus
	keyframeState.KeyframeTime = keyframeState.KeyframeDuration;
	keyframeState.KeyframeParameter = 1;
	clipState.ClipTime = clipState.CurClip->GetDuration();
	clipState.ClipParameter = 1;

	// pause
	clipState.PlaybackMode = EPlaybackMode::Pause;
}

void ClipTransitionImpl::ForwardSkipTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// continue from end of first keyframe
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetFirstKeyframeIndex());
	keyframeState.KeyframeTime = keyframeState.KeyframeDuration;
	keyframeState.KeyframeParameter = 1;
	// clip time is duration of first keyframe
	clipState.ClipTime = keyframeState.KeyframeDuration;
	clipState.ClipParameter = clipState.ClipTime * clipState.ClipDurationInv;
}

void ClipTransitionImpl::ForwardSkipPauseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// skip to end of first keyframe
	ForwardSkipTransition(clipState, keyframeState);
	// pause
	clipState.PlaybackMode = EPlaybackMode::Pause;
}

void ClipTransitionImpl::ReverseSkipTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// continue from start of last keyframe
	SetKeyframe(clipState, keyframeState,
		clipState.CurClip->GetLastKeyframeIndex());
	keyframeState.KeyframeTime = 0;
	keyframeState.KeyframeParameter = 0;
	// clip time is duration - last keyframe
	clipState.ClipTime = clipState.ClipDuration - keyframeState.KeyframeDuration;
	clipState.ClipParameter = clipState.ClipTime * clipState.ClipDurationInv;
}

void ClipTransitionImpl::ReverseSkipPauseTransition(FClipState& clipState, FKeyframeState& keyframeState)
{
	// skip to start of last keyframe
	ReverseSkipTransition(clipState, keyframeState);
	// pause
	clipState.PlaybackMode = EPlaybackMode::Pause;
}
