// Fill out your copyright notice in the Description page of Project Settings.


#include "ClipController.h"

ClipController::ClipController()
{
}

ClipController::~ClipController()
{
}

void ClipController::Init(KeyframePool* keyframePool, ClipPool* clipPool, uint32 initialClipIndex)
{
	mpKeyframePool = keyframePool;
	mpClipPool = clipPool;

	SetClip(initialClipIndex);
}

FKeyframeState& ClipController::Update(float DeltaTime)
{
	// apply speed multiplier
	DeltaTime *= Speed;
	
	// Pre-resolution, apply time step to keyframe and clip
	switch (mClipState.PlaybackMode)
	{
	// Case: Paused
	case EPlaybackMode::Pause:
	{
		break;
	}
	// Case: Forward
	case EPlaybackMode::Forward:
	{
		mKeyframeState.KeyframeTime += DeltaTime;
		mClipState.ClipTime += DeltaTime;
		// resolve
		ResolveForward();
		break;
	}
	// Case: Reverse
	case EPlaybackMode::Reverse:
	{
		mKeyframeState.KeyframeTime -= DeltaTime;
		mClipState.ClipTime -= DeltaTime;
		// resolve
		ResolveReverse();
		break;
	}
	}

	// Post-resolution, normalize time into parameter
	mKeyframeState.KeyframeParameter =
		mKeyframeState.KeyframeTime * mKeyframeState.KeyframeDurationInv;
	mClipState.ClipParameter =
		mClipState.ClipTime * mClipState.ClipDurationInv;

	// debug logging
	UE_LOG(LogTemp, Warning, TEXT("===================================ControllerName: %s ==============================="), *ControllerName);
	UE_LOG(LogTemp, Warning, TEXT("===================================CurClipName: %s ===================================="), *mClipState.CurClip->GetName());
	UE_LOG(LogTemp, Warning, TEXT("CurClipIndex: %i"), mClipState.CurClipIndex);
	UE_LOG(LogTemp, Warning, TEXT("CurClipTime: %f"), mClipState.ClipTime);
	UE_LOG(LogTemp, Warning, TEXT("CurClipDur: %f"), mClipState.ClipDuration);
	UE_LOG(LogTemp, Warning, TEXT("CurClipParam: %f"), mClipState.ClipParameter);
	UE_LOG(LogTemp, Warning, TEXT("CurKeyframeIndex: %i"), mKeyframeState.Keyframe0Index);
	UE_LOG(LogTemp, Warning, TEXT("CurKeyframeTime: %f"), mKeyframeState.KeyframeTime);
	UE_LOG(LogTemp, Warning, TEXT("CurKeyframeDur: %f"), mKeyframeState.KeyframeDuration);
	UE_LOG(LogTemp, Warning, TEXT("CurKeyframeParam: %f"), mKeyframeState.KeyframeParameter);
	Keyframe* curKeyframe = KeyframePool::GetInstance()->GetKeyframe(mKeyframeState.Keyframe0Index);
	UE_LOG(LogTemp, Warning, TEXT("CurKeyframeData: %i"), curKeyframe->GetValue());

	return mKeyframeState;
}

void ClipController::ResolveForward()
{
	// if time is over current keyframe interval
	if (mKeyframeState.KeyframeTime >= mKeyframeState.KeyframeDuration)
	{
		// set keyframe time to remainder over current keyframe
		mKeyframeState.KeyframeTime -= mKeyframeState.KeyframeDuration;

		// Case: Forward terminus, transition
		if (mClipState.ClipTime >= mClipState.ClipDuration)
		{
			// set clip time to remainder
			mClipState.ClipTime -= mClipState.ClipDuration;

			ClipTransitionImpl::Transition(mClipState, mKeyframeState);
		}
		else
		{
			// Case: Forward skip, advance to next keyframe
			ClipTransitionImpl::SetKeyframe(mClipState, mKeyframeState,
				mKeyframeState.Keyframe0Index + 1);
		}
		// recursive resolve in case new keyframe is shorter than the remainder
		// disabled because it breaks pausing at terminus
		//ResolveForward();
	}
}

void ClipController::ResolveReverse()
{
	// if time is before current keyframe interval
	if (mKeyframeState.KeyframeTime < 0)
	{
		// Case: Reverse terminus, transition
		if (mClipState.ClipTime < 0)
		{
			// set clip time to reverse remainder
			// because it's in reverse, cliptime IS the (negative) remainder,
			// so just add clipduration to get the new clip time
			mClipState.ClipTime += mClipState.ClipDuration;

			ClipTransitionImpl::Transition(mClipState, mKeyframeState);
		}
		else
		{
			// Case: reverse skip, advance to previous keyframe
			ClipTransitionImpl::SetKeyframe(mClipState, mKeyframeState,
				mKeyframeState.Keyframe0Index - 1);
		}

		// set keyframe time to reverse remainder, using new keyframe's duration
		mKeyframeState.KeyframeTime += mKeyframeState.KeyframeDuration;

		// recursive resolve in case new keyframe is shorter than the remainder
		// disabled because it breaks pausing at terminus
		//ResolveReverse();
	}
}

void ClipController::ResetToFirstKeyframe()
{
	ClipTransitionImpl::SetKeyframe(mClipState, mKeyframeState,
		mClipState.CurClip->GetFirstKeyframeIndex());
	mKeyframeState.KeyframeTime = 0;
	mKeyframeState.KeyframeParameter = 0;
	mClipState.ClipTime = 0;
	mClipState.ClipParameter = 0;
}

void ClipController::ResetToLastKeyframe()
{
	ClipTransitionImpl::SetKeyframe(mClipState, mKeyframeState,
		mClipState.CurClip->GetLastKeyframeIndex());
	// the only case where setting time to duration is legal
	mKeyframeState.KeyframeTime = mKeyframeState.KeyframeDuration;
	mKeyframeState.KeyframeParameter = 1;
	mClipState.ClipTime = mClipState.CurClip->GetDuration();
	mClipState.ClipParameter = 1;
}

void ClipController::SetClip(int32 clipIndex)
{
	check(clipIndex >= 0);

	mClipState.CurClipIndex = clipIndex;
	mClipState.CurClip = mpClipPool->GetClip(mClipState.CurClipIndex);
	mClipState.ClipDuration = mClipState.CurClip->GetDuration();
	mClipState.ClipDurationInv = mClipState.CurClip->GetInverseDuration();

	// reset keyframe state to first keyframe in selected clip
	ResetToFirstKeyframe();
}

void ClipController::SetSpeed(float speed)
{
	check(speed >= 0);

	Speed = speed;
}
