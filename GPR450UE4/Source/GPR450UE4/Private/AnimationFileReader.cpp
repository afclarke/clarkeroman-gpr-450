// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimationFileReader.h"
#include "KeyframePool.h"
#include "ClipPool.h"


AAnimationDataReader::AAnimationDataReader()
{
}

AAnimationDataReader::~AAnimationDataReader()
{
}

void AAnimationDataReader::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

}

void AAnimationDataReader::Init(UDataTable* animationData, KeyframePool* keyframePool, ClipPool* clipPool)
{
	AnimationData = animationData;
	KeyframePoolInstance = keyframePool;
	ClipPoolInstance = clipPool;

	CreateClips();
}


void AAnimationDataReader::CreateClips()
{
	//Read Data
	ensureAlways(AnimationData);
	ensureAlways(KeyframePoolInstance);
	ensureAlways(ClipPoolInstance);
	TArray<FAnimationData*> AllRows;
	FString Context;
	AnimationData->GetAllRows<FAnimationData>(Context, AllRows);

	for (int i = 0; i < AllRows.Num(); i++)
	{
		FAnimationData* row = AllRows[i];

		FClipTransition transition(row->TransitionAction, row->TransitionToClip);

		Clip* NewClip = new Clip(row->ClipName, row->FirstKeyframe, row->LastKeyframe, transition, KeyframePoolInstance);
		ClipPoolInstance->AddClip(NewClip);

		UE_LOG(LogTemp, Warning, TEXT("New Clip: %s , %i , %i"), *row->ClipName, row->FirstKeyframe, row->LastKeyframe);
	}
}

