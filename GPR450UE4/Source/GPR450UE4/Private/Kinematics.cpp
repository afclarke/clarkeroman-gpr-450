//// Fill out your copyright notice in the Description page of Project Settings.
//
//
//#include "Kinematics.h"
//#include "HierarchyState.h"
//
//void Kinematics::KinematicsSolveForward(const HierarchyState* hierarchyState)
//{
//	KinematicsSolveForwardPartial(hierarchyState, 0, hierarchyState->pHierarchyPoseGroup->Nodes.Num());
//}
//
//void Kinematics::KinematicsSolveForwardPartial(const HierarchyState* hierarchyState, const uint32 firstIndex, const uint32 nodeCount)
//{
//	if (hierarchyState && hierarchyState->pHierarchyPoseGroup &&
//		firstIndex < (uint32)hierarchyState->pHierarchyPoseGroup->Nodes.Num() && nodeCount)
//	{
//		// ****TO-DO: implement forward kinematics algorithm
//		//	- for all nodes starting at first index
//		//		- if node is not root (has parent node)
//		//			- object matrix = parent object matrix * local matrix
//		//		- else
//		//			- copy local matrix to object matrix
//		HierarchyNode* node;
//		const Hierarchy* hierarchy = hierarchyState->pHierarchyPoseGroup;
//
//		for (uint32 i = firstIndex; i < nodeCount; ++i)
//		{
//			node = hierarchyState->pHierarchyPoseGroup->Nodes[i];
//
//			if (!hierarchy->IsChildNode(i, 0)) // If not a root node
//			{
//
//			}
//		}
//	}
//}
//
//void Kinematics::KinematicsSolveInverse(const HierarchyState* hierarchyState)
//{
//	KinematicsSolveInversePartial(hierarchyState, 0, hierarchyState->HierarchyPtr->Nodes.Num());
//}
//
//void Kinematics::KinematicsSolveInversePartial(const HierarchyState* hierarchyState, const uint32 firstIndex, const uint32 nodeCount)
//{
//	if (hierarchyState && hierarchyState->HierarchyPtr &&
//		firstIndex < (uint32)hierarchyState->HierarchyPtr->Nodes.Num() && nodeCount)
//	{
//		// ****TO-DO: implement inverse kinematics algorithm
//		//	- for all nodes starting at first index
//		//		- if node is not root (has parent node)
//		//			- local matrix = inverse parent object matrix * object matrix
//		//		- else
//		//			- copy object matrix to local matrix
//	}
//}
