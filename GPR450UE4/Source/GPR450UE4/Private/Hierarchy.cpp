// Fill out your copyright notice in the Description page of Project Settings.


#include "Hierarchy.h"


Hierarchy::Hierarchy(int32 nodeCount)
{
	Nodes.SetNumUninitialized(nodeCount);
}

Hierarchy::~Hierarchy()
{
	for (int32 i = 0; i < Nodes.Num(); i++)
	{
		if (Nodes[i])
		{
			delete Nodes[i];
		}
	}
}

int32 Hierarchy::SetNode(const uint32 index, const int32 parentIndex, const FString& name)
{
	if (index < (uint32)Nodes.Num())
	{
		if ((int32)index > parentIndex)
		{
			Nodes[index] = new HierarchyNode(name, index, parentIndex);
			return index;
		}
	}
	return -1;
}

int32 Hierarchy::GetNodeIndexFromName(const FString& name) const
{
	// GLOBAL/ROOT is -1
	if (name == "GLOBAL")
	{
		return -1;
	}

	for (int32 i = 0; i < Nodes.Num(); ++i)
	{
		if (Nodes[i]->Name == name)
		{
			return i;
		}
	}
	return -1;
}

const TArray<FString> Hierarchy::GetNodeNames()
{
	TArray<FString> nodeNames;

	for (int32 i = 0; i < Nodes.Num(); ++i)
	{
		nodeNames.Add(Nodes[i]->Name);
	}
	return nodeNames;
}

bool Hierarchy::IsParentNode(const uint32 parentIndex, const uint32 otherIndex) const
{
	if (Nodes.IsValidIndex(otherIndex))
	{
		return Nodes[otherIndex]->ParentIndex == parentIndex;
	}
	return false;
}

bool Hierarchy::IsChildNode(const uint32 childIndex, const uint32 otherIndex) const
{
	return IsParentNode(otherIndex, childIndex);
}

bool Hierarchy::IsSiblingNode(const uint32 siblingIndex, const uint32 otherIndex) const
{
	if (Nodes.IsValidIndex(otherIndex) && Nodes.IsValidIndex(otherIndex))
	{
		return (Nodes[otherIndex]->ParentIndex == Nodes[siblingIndex]->ParentIndex);
	}
	return false;

}

bool Hierarchy::IsAncestorNode(const uint32 ancestorIndex, const uint32 otherIndex) const
{
	uint32 i = otherIndex;
	if (Nodes.IsValidIndex(otherIndex) && Nodes.IsValidIndex(ancestorIndex))
	{
		while (i > ancestorIndex)
		{
			i = Nodes[i]->ParentIndex;
		}
		return (i == ancestorIndex);
	}
	return false;
}

bool Hierarchy::IsDescendantNode(const uint32 descendantIndex, const uint32 otherIndex) const
{
	return IsAncestorNode(otherIndex, descendantIndex);
}

//void Hierarchy::SaveBinary(const a3_FileStream* fileStream)
//{
//}
//
//void Hierarchy::LoadBinary(const a3_FileStream* fileStream)
//{
//}

void Hierarchy::CopyToString(uint8* str)
{
}

void Hierarchy::CopyFromString(const uint8* str)
{
}

void Hierarchy::GetStringSize()
{
}

void Hierarchy::Release()
{
}
