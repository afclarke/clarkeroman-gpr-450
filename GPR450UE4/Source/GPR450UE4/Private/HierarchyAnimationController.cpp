// Fill out your copyright notice in the Description page of Project Settings.


#include "HierarchyAnimationController.h"
#include "HierarchyFileImporter.h"
#include "DrawDebugHelpers.h"

static FVector GetChainRotDirForward(FVector chainDir)
{
	return chainDir;
}
static FVector GetChainRotDirBackward(FVector chainDir)
{
	return (-chainDir);
}
static FVector GetChainRotDirLeft(FVector chainDir)
{
	chainDir = FVector::CrossProduct(FVector::DownVector, chainDir);
	return chainDir;
}

// Sets default values for this component's properties
UHierarchyAnimationController::UHierarchyAnimationController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UHierarchyAnimationController::BeginPlay()
{
	Super::BeginPlay();

	mOrigin = LocationRotation(GetOwner()->GetActorTransform());
	InitHierarchy();
	InitBlendTree();
	LeftArmPlaneConstraintTransform = Cast<USceneComponent>(GetOwner()->GetComponentByClass(USceneComponent::StaticClass()));

	//const int32 neckNodeIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("Neck"));
	//SpatialPose* neckBaseSpatialPose = mpHierarchyState->pHierarchyPoseGroup->mpBasePose->Pose[neckNodeIndex];
	//WanderTarget = neckBaseSpatialPose->Translation;
	firstWander = false;
	neckWanderVel = FVector::ZeroVector;
	leftArmWanderVel = FVector::ZeroVector;
	rightArmWanderVel = FVector::ZeroVector;
}

void UHierarchyAnimationController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	for (int i = 0; i < mJointActors.Num(); i++)
	{
		mJointActors[i]->Destroy();
	}
	CleanupHierarchy();
	CleanupBlendTree();
}

void UHierarchyAnimationController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	for (int32 i = 0; i < mClipControllers.Num(); ++i)
	{
		mClipControllers[i].Update(DeltaTime);
		mPoseFrames[i].Update();
	}

	// interp, concat, convert
	mpBlendNodeTree->ExecTree();

	// run locomotion on actor root
	LocomotionLinear(mMoveInput.Linear, DeltaTime);
	LocomotionAngular(mMoveInput.Angular, DeltaTime);
	SpatialPose actorRootPose = SpatialPose::Construct(GetOwner()->GetTransform());

	// FK
	HierarchyPose::ForwardKinematics(mpHierarchyState->WorldSpacePose, mpHierarchyState->ObjectSpacePose, mpHierarchyState->LocalSpacePose, mpHierarchyPoseGroup->HierarchyPtr, &actorRootPose);

	const int32 neckNodeIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("Neck"));
	SpatialPose* neckObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[neckNodeIndex];
	SpatialPose::Revert(neckObjSpatialPose);
	const int32 hipsNodeIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("Hips"));

	if(!firstWander)
	{
		SpatialPose::Copy(&neckEndEffectorSpatialPose, neckObjSpatialPose);
		neckWanderTarget = neckEndEffectorSpatialPose.Translation;
	}
	
	WanderEndEffectorTranslation(neckEndEffectorSpatialPose, neckWanderTarget, neckObjSpatialPose->Translation, NeckWanderRadius, WanderTolerance, NeckWanderMaxVel, neckWanderVel, DeltaTime);
	UnconstrainedIK(neckNodeIndex, hipsNodeIndex, neckEndEffectorSpatialPose, &GetChainRotDirLeft, 20, DeltaTime);
	
	const int32 leftHandIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("LeftHand"));
	SpatialPose* leftHandObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[leftHandIndex];
	SpatialPose::Revert(leftHandObjSpatialPose);
	const int32 leftShoulderIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("LeftShoulder"));

	if (!firstWander)
	{
		SpatialPose::Copy(&leftArmEndEffectorSpatialPose, leftHandObjSpatialPose);
		leftArmWanderTarget = leftArmEndEffectorSpatialPose.Translation;
	}
	
	WanderEndEffectorTranslation(leftArmEndEffectorSpatialPose, leftArmWanderTarget, leftHandObjSpatialPose->Translation, ArmWanderRadius, WanderTolerance, ArmWanderMaxVel, leftArmWanderVel, DeltaTime);
	UnconstrainedIK(leftHandIndex, leftShoulderIndex, leftArmEndEffectorSpatialPose, &GetChainRotDirForward, 20, DeltaTime);

	const int32 rightHandIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("RightHand"));
	SpatialPose* rightHandObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[rightHandIndex];
	SpatialPose::Revert(rightHandObjSpatialPose);
	const int32 rightShoulderIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("RightShoulder"));

	if (!firstWander)
	{
		SpatialPose::Copy(&rightArmEndEffectorSpatialPose, rightHandObjSpatialPose);
		rightArmWanderTarget = rightArmEndEffectorSpatialPose.Translation;
	}
	firstWander = true;

	WanderEndEffectorTranslation(rightArmEndEffectorSpatialPose, rightArmWanderTarget, rightHandObjSpatialPose->Translation, ArmWanderRadius, WanderTolerance, ArmWanderMaxVel, rightArmWanderVel, DeltaTime);
	UnconstrainedIK(rightHandIndex, rightShoulderIndex, rightArmEndEffectorSpatialPose, &GetChainRotDirBackward, 20, DeltaTime);

	//LeftArmIK();
	//NeckIK();

	//Run FK again to update object space
	HierarchyPose::ForwardKinematics(mpHierarchyState->WorldSpacePose, mpHierarchyState->ObjectSpacePose, mpHierarchyState->LocalSpacePose, mpHierarchyPoseGroup->HierarchyPtr, &actorRootPose);

	// update to joint actors
	for (int i = 0; i < mJointActors.Num(); i++)
	{
		// apply to actors
		mJointActors[i]->SetActorTransform(mpHierarchyState->WorldSpacePose->Pose[i]->Transform);
		//mJointActors[i]->SetActorTransform(mpHierarchyState->ObjectSpacePose->Pose[i]->Transform);
	}
}

void UHierarchyAnimationController::NeckIK()
{
	//IK For Neck node
	//Get index and joint of the neck
	const int32 neckNodeIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("Neck"));
	SpatialPose* neckSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[neckNodeIndex];
	//Update it's translation
	SpatialPose::Revert(neckSpatialPose);

	//Get the vector between the neck and its target
	FVector lookVector = LookTarget->GetActorLocation() - neckSpatialPose->Translation;
	//Create a new copy of neck spatial pose with target rotation
	SpatialPose targetPose;
	SpatialPose::Copy(&targetPose, neckSpatialPose);

	lookVector = lookVector.ToOrientationRotator().Euler();

	//SpatialPose temp;
	//SpatialPose::Copy(&temp, neckSpatialPose);
	//temp.SetRotation(FVector(lookVector.X, lookVector.Y, lookVector.Z - 90));
	neckSpatialPose->SetRotation(FVector(lookVector.X, lookVector.Y, lookVector.Z - 90));
	//SpatialPose::Convert(&temp);
	SpatialPose::Convert(neckSpatialPose);

	HierarchyNode* neckNode = mpHierarchyState->pHierarchyPoseGroup->HierarchyPtr->Nodes[neckNodeIndex];
	SpatialPose::InverseKinematics(mpHierarchyState->LocalSpacePose->Pose[neckNodeIndex],
		neckSpatialPose, mpHierarchyState->ObjectSpacePose->Pose[neckNode->ParentIndex]);
}

void UHierarchyAnimationController::LeftArmIK()
{
	//IK For left arm
	// get hand, elbow, and shoulder nodes & obj space poses
	const int32 leftHandNodeIndex = mpHierarchyPoseGroup->HierarchyPtr->GetNodeIndexFromName(FString("LeftHand"));
	HierarchyNode* leftHandNode = mpHierarchyState->pHierarchyPoseGroup->HierarchyPtr->Nodes[leftHandNodeIndex];
	SpatialPose* leftHandObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[leftHandNodeIndex];
	SpatialPose* leftHandBaseSpatialPose = mpHierarchyState->pHierarchyPoseGroup->mpBasePose->Pose[leftHandNodeIndex];
	int32 leftElbowNodeIndex = leftHandNode->ParentIndex;
	SpatialPose* leftElbowObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[leftElbowNodeIndex];
	SpatialPose* leftElbowBaseSpatialPose = mpHierarchyState->pHierarchyPoseGroup->mpBasePose->Pose[leftElbowNodeIndex];
	HierarchyNode* leftElbowNode = mpHierarchyState->pHierarchyPoseGroup->HierarchyPtr->Nodes[leftElbowNodeIndex];
	SpatialPose* leftShoulderObjSpatialPose = mpHierarchyState->ObjectSpacePose->Pose[leftElbowNode->ParentIndex];

	// create spatial pose from end effector
	SpatialPose leftHandEndEffectorPose = SpatialPose::Construct(
		LeftHandEndEffectorActor->GetActorRotation().Euler(), LeftHandEndEffectorActor->GetActorLocation(), LeftHandEndEffectorActor->GetActorScale());
	SpatialPose::Revert(leftShoulderObjSpatialPose);

	// d = end effector - shoulder
	FVector d = leftHandEndEffectorPose.Translation - leftShoulderObjSpatialPose->Translation;
	const float dLen = d.Size();

	// calc arm length
	float worldspaceScale = GetOwner()->GetActorScale3D().X;
	float ArmL1 = leftElbowBaseSpatialPose->BoneLength * worldspaceScale;
	float ArmL2 = leftHandBaseSpatialPose->BoneLength * worldspaceScale;
	const float armLength = ArmL1 + ArmL2;

	// only apply IK for hand and elbow within range of arm
	if (dLen < armLength)
	{
		// set left hand obj space loc to end effector obj space loc
		SpatialPose::Revert(leftHandObjSpatialPose);
		leftHandObjSpatialPose->SetTranslation(leftHandEndEffectorPose.Translation);
		SpatialPose::Convert(leftHandObjSpatialPose);

		//Constraint displacement
		FVector c = LeftArmPlaneConstraintTransform->GetComponentLocation() - leftShoulderObjSpatialPose->Translation;
		//Constraint plane normal
		FVector n = FVector::CrossProduct(d, c);
		n.Normalize();
		d.Normalize();
		//Normal in direction of the constraint triangle height
		FVector h = FVector::CrossProduct(n, d).GetSafeNormal();
		//Herons Formula
		float s = 0.5 * (dLen + armLength);
		float A = FMath::Sqrt(s * (s - dLen) * (s - ArmL1) * (s - ArmL2));
		//Height of elbow joint triangle
		float H = 2 * A / dLen;
		//Pythagorean Theron
		float D = FMath::Sqrt(ArmL1 * ArmL1 - H * H);
		//Final Elbow Position 
		FVector P = leftShoulderObjSpatialPose->Translation + D * d + H * h;

		//Set rotation of elbow to constraint plane normal
		SpatialPose::Revert(leftElbowObjSpatialPose);
		leftElbowObjSpatialPose->SetTranslation(P);
		leftElbowObjSpatialPose->SetRotation(n.ToOrientationRotator().Euler());
		SpatialPose::Convert(leftElbowObjSpatialPose);

		// IK for local left hand pose
		SpatialPose::InverseKinematics(mpHierarchyState->LocalSpacePose->Pose[leftHandNodeIndex], leftHandObjSpatialPose, leftElbowObjSpatialPose);
		// IK for local elbow space pose
		SpatialPose::InverseKinematics(mpHierarchyState->LocalSpacePose->Pose[leftElbowNodeIndex], leftElbowObjSpatialPose, leftShoulderObjSpatialPose);
	}
}

PRAGMA_DISABLE_OPTIMIZATION
void UHierarchyAnimationController::UnconstrainedIK(int32 endJointIndex, int32 baseJointIndex, SpatialPose& endEffectorSpatialPose, FVector(*getChainRotDir)(FVector chainDir), int32 maxLoops, float dt)
{
	// [0]= endJoint, [numNodes] = baseJoint
	TArray<HierarchyNode*> nodes;
	TArray<SpatialPose*> objSpatialPoses;
	TArray<SpatialPose*> localSpatialPoses;
	TArray<SpatialPose*> baseSpatialPoses;
	
	int32 numNodes = 0;
	int32 newNodeIndex = endJointIndex;
	int32 baseJointParentIndex = mpHierarchyState->pHierarchyPoseGroup->HierarchyPtr->Nodes[baseJointIndex]->ParentIndex;
	do
	{
		nodes.Add(mpHierarchyState->pHierarchyPoseGroup->HierarchyPtr->Nodes[newNodeIndex]);
		objSpatialPoses.Add(mpHierarchyState->ObjectSpacePose->Pose[newNodeIndex]);
		SpatialPose::Revert(objSpatialPoses[numNodes]);
		localSpatialPoses.Add(mpHierarchyState->LocalSpacePose->Pose[newNodeIndex]);
		baseSpatialPoses.Add(mpHierarchyState->pHierarchyPoseGroup->mpBasePose->Pose[newNodeIndex]);
		
		newNodeIndex = nodes[numNodes]->ParentIndex;
		numNodes++;
	
	} while (newNodeIndex != baseJointParentIndex);

	SpatialPose::Revert(&endEffectorSpatialPose);
	float lenToEndEffector;
	int numLoops = 0;
	do
	{
		objSpatialPoses[0]->SetTranslation(endEffectorSpatialPose.Translation);
		SpatialPose::Convert(objSpatialPoses[0]);

		// chain from end/neck to base/hips
		FVector chainDirInv;
		// second to end node to second to base node
		for(int32 i = 1; i <= numNodes - 2; i++)
		{
			int32 childIndex = i - 1;
			// vector normal pointing from child to parent in chain
			chainDirInv = (objSpatialPoses[i]->Translation - objSpatialPoses[childIndex]->Translation).GetSafeNormal();
			// child offset by bone length in chain dir
			objSpatialPoses[i]->SetTranslation(objSpatialPoses[childIndex]->Translation + chainDirInv * baseSpatialPoses[childIndex]->BoneLength);
		}

		// chain to base effector
		FVector chainDir;
		// from second from base node to end node
		for(int32 i = numNodes - 2; i >= 0; i--)
		{
			int32 parentIndex = i + 1;
			chainDir = (objSpatialPoses[i]->Translation - objSpatialPoses[parentIndex]->Translation).GetSafeNormal();
			objSpatialPoses[i]->SetTranslation(objSpatialPoses[parentIndex]->Translation + chainDir *  baseSpatialPoses[i]->BoneLength);
			objSpatialPoses[i]->SetRotation(getChainRotDir(chainDir).ToOrientationRotator().Euler());
		}

		lenToEndEffector = (objSpatialPoses[0]->Translation - endEffectorSpatialPose.Translation).Size();

	} while (++numLoops < maxLoops && lenToEndEffector > SpineTolerance);

	// IK on all but base
	for(int32 i = 0; i < numNodes - 1; i++)
	{
		SpatialPose::Convert(objSpatialPoses[i]);
		SpatialPose::InverseKinematics(localSpatialPoses[i], objSpatialPoses[i], objSpatialPoses[i + 1]);
	}

	nodes.Empty();
	objSpatialPoses.Empty();
	localSpatialPoses.Empty();
	baseSpatialPoses.Empty();
}

PRAGMA_DISABLE_OPTIMIZATION
void UHierarchyAnimationController::WanderEndEffectorTranslation(SpatialPose& endEffectorTargetTranslation, FVector& wanderTarget,
	FVector wanderCenter, float wanderRadius, float wanderTolerance, float wanderMaxVel, FVector& wanderVel, float dt)
{
	FVector directionVec = wanderTarget - endEffectorTargetTranslation.Translation;
	float distance = directionVec.Size();

	if (distance < wanderTolerance)
	{
		//Chose new point
		FVector2D randPoint = FMath::RandPointInCircle(wanderRadius);
		wanderTarget = FVector(wanderCenter.X + randPoint.X, wanderCenter.Y, wanderCenter.Z + randPoint.Y);
		directionVec = wanderTarget - endEffectorTargetTranslation.Translation;
	}

	// integrate velocity for face accel
	wanderVel = FMath::Lerp(wanderVel, directionVec.GetSafeNormal() * wanderMaxVel, dt);
	endEffectorTargetTranslation.Translation += wanderVel * dt;

	SpatialPose::Convert(&endEffectorTargetTranslation);
	DrawDebugLine(GetWorld(), endEffectorTargetTranslation.Translation, wanderTarget,
		FColor::Green, false, -1, 0, 5);
}
PRAGMA_ENABLE_OPTIMIZATION

void UHierarchyAnimationController::InitHierarchy()
{
	HierarchyFileImporter::LoadHTR(BasePoseHtrPath, mpHierarchyPoseGroup, true);

	for (int32 i = 0; i < HtrPaths.Num(); i++)
	{
		HTRHeader header = HierarchyFileImporter::LoadHTR(HtrPaths[i], mpHierarchyPoseGroup);
		KeyframePool* pKeyframePool = KeyframePool::GetInstance();
		ClipPool* pClipPool = ClipPool::GetInstance();
		int32 newClipIndex = pClipPool->GetClipIndex(header.Name);
		// clip for this htr already exists
		if (newClipIndex >= 0)
		{
			newClipIndex = pClipPool->GetClipIndex(header.Name);
		}
		// create keyframes for each htr frame, create new clip from keyframes
		else
		{
			int32 firstkeyframeindex = pKeyframePool->GetNumKeyframes();
			int32 lastKeyframeIndex = firstkeyframeindex;
			int32 totalNumPoses = mpHierarchyPoseGroup->HierarchicalPoses.Num();
			int32 firstPoseIndex = totalNumPoses - header.NumFrames;
			for (int32 poseIndex = firstPoseIndex; poseIndex < totalNumPoses; poseIndex++)
			{
				float poseDuration = 1.0f / header.DataFrameRate;
				lastKeyframeIndex = pKeyframePool->AddKeyframe(new Keyframe(poseDuration, poseIndex));
			}
			newClipIndex = pClipPool->AddClip(
				new Clip(header.Name, firstkeyframeindex, lastKeyframeIndex,
					FClipTransition(ETransitionAction::Forward, header.Name), pKeyframePool));
		}
		// new clip controller
		mClipControllers.Add(ClipController());
		mClipControllers.Last().Init(pKeyframePool, pClipPool, newClipIndex);
		mClipControllers.Last().SetPlaybackMode(EPlaybackMode::Forward);
		mPoseFrames.Add(PoseFrame(mpHierarchyPoseGroup, &mClipControllers.Last().mKeyframeState));
	}

	// to be replaced by loaded base pose mesh
	// spawn actors in level which HierarchyState will use to display sample pose
	check(JointActorClass);
	int32 numJoints = mpHierarchyPoseGroup->HierarchyPtr->Nodes.Num();
	mJointActors.SetNumUninitialized(numJoints);
	for (int32 i = 0; i < numJoints; i++)
	{
		mJointActors[i] = GetWorld()->SpawnActor<AActor>(JointActorClass);
	}

	// finally set up HierarchyPtr states
	mpHierarchyState = new HierarchyState(mpHierarchyPoseGroup);
}

void UHierarchyAnimationController::CleanupHierarchy()
{
	for (int32 i = 0; i < mJointActors.Num(); i++)
	{
		mJointActors[i]->Destroy();
	}
	mJointActors.Empty();
	mPoseFrames.Empty();
	delete mpHierarchyState;
	delete mpHierarchyPoseGroup;
	mClipControllers.Empty();
}

void UHierarchyAnimationController::InitBlendTree()
{
	mpBlendNodeTree = new BlendNodeTree();

	// concat
	BlendNode* concatBlendNode = new BlendNode(&BlendNode::ExecConcat,
		mpHierarchyState->LocalSpacePose,
		{ &mpHierarchyPoseGroup->mpBasePose, &mpHierarchyState->DeltaPose },
		{});
	mpBlendNodeTree->AddNode(concatBlendNode);

	// convert
	BlendNode* convertBlendNode = new BlendNode(&BlendNode::ExecConvert,
		mpHierarchyState->LocalSpacePose, {}, {});
	mpBlendNodeTree->AddNode(convertBlendNode);

	int32 numHierarchyNodes = mpHierarchyState->NumNodes;

	// idle
	mpIdleSmoothstepPose = new HierarchyPose(numHierarchyNodes);
	BlendNode* idleSmoothstepNode = new BlendNode(&BlendNode::ExecSmoothStep,
		mpIdleSmoothstepPose,
		{ &mPoseFrames[idleClipIndex].p0, &mPoseFrames[idleClipIndex].p1, },
		{ &mClipControllers[idleClipIndex].mKeyframeState.KeyframeParameter });
	mpBlendNodeTree->AddNode(idleSmoothstepNode);

	// run
	mpRunSmoothstepPose = new HierarchyPose(numHierarchyNodes);
	BlendNode* runSmoothstepNode = new BlendNode(&BlendNode::ExecSmoothStep,
		mpRunSmoothstepPose,
		{ &mPoseFrames[runClipIndex].p0, &mPoseFrames[runClipIndex].p1, },
		{ &mClipControllers[runClipIndex].mKeyframeState.KeyframeParameter });
	mpBlendNodeTree->AddNode(runSmoothstepNode);

	// idle run
	BlendNode* idleRunLerpNode = new BlendNode(&BlendNode::ExecLinear,
		mpHierarchyState->DeltaPose,
		{ &mpIdleSmoothstepPose, &mpRunSmoothstepPose, },
		{ &mVelocityLinearPercent });
	mpBlendNodeTree->AddNode(idleRunLerpNode);
}

void UHierarchyAnimationController::CleanupBlendTree()
{
	delete mpBlendNodeTree;
}

void UHierarchyAnimationController::LocomotionLinear(FVector inputLinear, float dt)
{
	FVector curLocation = GetOwner()->GetActorLocation();
	switch (LocomotionModeLinear)
	{
	case ELocomotionMode::Direct:
	{
		curLocation = mOrigin.Linear;
		curLocation += inputLinear * LocomotionLerpPosLinearRadius;
		break;
	}
	case ELocomotionMode::Euler:
	{
		mVelocity.Linear = inputLinear * LocomotionEulerVelocityLinear;
		curLocation = IntegrateEuler(curLocation, mVelocity.Linear, dt);
		break;
	}
	case ELocomotionMode::Kinematic:
	{
		mAccel.Linear = inputLinear * LocomotionKinematicAccelLinear;
		mVelocity = mVelocity + mAccel * dt;
		curLocation = IntegrateKinematic(curLocation, mVelocity.Linear, mAccel.Linear, dt);
		break;
	}
	// fake velocity by lerping position/rotation from cur to target
	case ELocomotionMode::LerpPos:
	{
		FVector targetLocation = mOrigin.Linear;
		targetLocation += inputLinear * LocomotionLerpPosLinearRadius;
		curLocation = IntegrateLerp(curLocation, targetLocation, dt);
		break;
	}
	// face acceleration by lerping velocity from cur to target
	case ELocomotionMode::LerpVel:
	{
		// integrate pos/rot from vel 
		curLocation = IntegrateEuler(curLocation, mVelocity.Linear, dt);
		// lerp velocity to input target
		FVector targetVelocity = inputLinear * LocomotionLerpVelLinearRadius;
		mVelocity.Linear = IntegrateLerp(mVelocity.Linear, targetVelocity, dt);
		break;
	}
	}
	GetOwner()->SetActorLocation(curLocation);
	mVelocityLinearPercent = mVelocity.Linear.Size() / LocomotionEulerVelocityLinear;
}

void UHierarchyAnimationController::LocomotionAngular(FVector inputAngular, float dt)
{
	FVector curRotation = GetOwner()->GetActorRotation().Euler();
	switch (LocomotionModeAngular)
	{
	case ELocomotionMode::Direct:
	{
		curRotation = mOrigin.Angular;
		curRotation += inputAngular * LocomotionLerpPosAngularRadius;
		break;
	}
	case ELocomotionMode::Euler:
	{
		mVelocity.Angular = inputAngular * LocomotionEulerVelocityAngular;
		curRotation = IntegrateEuler(curRotation, mVelocity.Angular, dt);
		break;
	}
	case ELocomotionMode::Kinematic:
	{
		mAccel.Angular = inputAngular * LocomotionKinematicAccelAngular;
		mVelocity = mVelocity + mAccel * dt;
		curRotation = IntegrateKinematic(curRotation, mVelocity.Angular, mAccel.Angular, dt);
		break;
	}
	// fake velocity by lerping position/rotation from cur to target
	case ELocomotionMode::LerpPos:
	{
		FVector targetLocation = mOrigin.Angular;
		targetLocation += inputAngular * LocomotionLerpPosAngularRadius;
		curRotation = IntegrateLerp(curRotation, targetLocation, dt);
		break;
	}
	// face acceleration by lerping velocity from cur to target
	case ELocomotionMode::LerpVel:
	{
		// integrate pos/rot from vel 
		curRotation = IntegrateEuler(curRotation, mVelocity.Angular, dt);
		// lerp velocity to input target
		FVector targetVelocity = inputAngular * LocomotionLerpVelAngularRadius;
		mVelocity.Angular = IntegrateLerp(mVelocity.Angular, targetVelocity, dt);
		break;
	}
	}
	GetOwner()->SetActorRotation(FRotator::MakeFromEuler(curRotation));
	//mVelocityAngularMagnitude = mVelocity.Angular.Size();
}

FVector UHierarchyAnimationController::IntegrateEuler(const FVector& x, const FVector& dx_dt, float dt)
{
	return  x + dx_dt * dt;
}

FVector UHierarchyAnimationController::IntegrateKinematic(const FVector& x, const FVector& dx_dt, const FVector& dx_dt2,
	float dt)
{
	return x + dx_dt * dt + dx_dt2 * dt * dt / 2.0f;
}

FVector UHierarchyAnimationController::IntegrateLerp(const FVector& x, const FVector& x_f, float u)
{
	return x * (1.0f - u) + x_f * u;
}
