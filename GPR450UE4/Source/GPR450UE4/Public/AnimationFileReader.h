// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ClipTransition.h"
#include "AnimationFileReader.generated.h"


USTRUCT(BlueprintType)
struct FAnimationData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FAnimationData()
		: ClipName("")
		, FirstKeyframe(0)
		, LastKeyframe(0)
		, TransitionAction()
		, TransitionToClip()
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimationSaveData)
	FString ClipName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimationSaveData)
	int32 FirstKeyframe;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimationSaveData)
	int32 LastKeyframe;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimationSaveData)
	ETransitionAction TransitionAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AnimationSaveData)
	FString TransitionToClip;
};

class ClipPool;
class KeyframePool;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GPR450UE4_API AAnimationDataReader : public AActor
{
	GENERATED_BODY()
public:
	AAnimationDataReader();
	~AAnimationDataReader();

	void Init(UDataTable* AnimationData, KeyframePool* KeyframePool, ClipPool* ClipPool);

private:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void CreateClips();

private:

	UDataTable* AnimationData;

	KeyframePool* KeyframePoolInstance;

	ClipPool* ClipPoolInstance;
};