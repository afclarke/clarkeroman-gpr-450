// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
struct LocationRotation
{
	LocationRotation()
	{
		Linear = FVector::ZeroVector;
		Angular = FVector::ZeroVector;
	}

	LocationRotation(FTransform transform)
	{
		Linear = transform.GetLocation();
		Angular = transform.GetRotation().Euler();
	}

	FVector Linear;
	// in degrees
	FVector Angular;
};

inline LocationRotation operator+(const LocationRotation& lhs, const LocationRotation& rhs)
{
	LocationRotation sum;
	sum.Linear = lhs.Linear + rhs.Linear;
	sum.Angular = lhs.Angular + rhs.Angular;
	return sum;
}

inline LocationRotation operator-(const LocationRotation& lhs, const LocationRotation& rhs)
{
	LocationRotation diff;
	diff.Linear = lhs.Linear - rhs.Linear;
	diff.Angular = lhs.Angular - rhs.Angular;
	return diff;
}

inline LocationRotation operator*(const LocationRotation& lhs, float scalar)
{
	LocationRotation product;
	product.Linear = lhs.Linear * scalar;
	product.Angular = lhs.Angular * scalar;
	return product;
}

inline LocationRotation operator/(const LocationRotation& lhs, float divisor)
{
	LocationRotation quotient;
	quotient.Linear = lhs.Linear / divisor;
	quotient.Angular = lhs.Angular / divisor;
	return quotient;
}
