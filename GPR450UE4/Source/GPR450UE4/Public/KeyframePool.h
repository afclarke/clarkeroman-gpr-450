// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Keyframe.h"

/**
 * An unordered, unsorted collection of keyframes.
 */
class KeyframePool
{
public:
	// lazy get instance
	static KeyframePool* GetInstance();
	static void Destroy() { delete GetInstance(); }
	Keyframe* GetKeyframe(int32 index);

	int32 AddKeyframe(Keyframe* newKeyframe);
	int32 GetNumKeyframes() { return Keyframes.Num(); }
private:
	KeyframePool();
	~KeyframePool();
private:
	TArray<Keyframe*> Keyframes;

	// singleton
	static KeyframePool* instance;
};
