// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 *
 */

class HierarchyNode
{
public:
	HierarchyNode(FString name, uint32 index, int32 parentIndex)
		: Name(name), Index(index), ParentIndex(parentIndex) { }
	~HierarchyNode() {}
public:
	FString Name;
	uint32 Index;
	int32 ParentIndex;
};
