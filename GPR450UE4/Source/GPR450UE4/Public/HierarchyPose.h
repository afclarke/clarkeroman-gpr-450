// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpatialPose.h"

class Hierarchy;

/**
 * 
 */
class GPR450UE4_API HierarchyPose
{
public:
	HierarchyPose(uint32 numNodes);
	//HierarchyPose(const HierarchyPose& rhs);
	~HierarchyPose();

	static HierarchyPose* Step(HierarchyPose* p_out, HierarchyPose* p0);
	static HierarchyPose* Nearest(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u);
	static HierarchyPose* Linear(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u);
	static HierarchyPose* Smoothstep(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u);
	static HierarchyPose* Smootherstep(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, float u);
	static HierarchyPose* Cubic(HierarchyPose* p_out, HierarchyPose* pP, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* pN, float u);
	
	//void Triangular(HierarchyPose* deltaPose, int32 pose0Index, int32 pose1Index, int32 poseNIndex, float u);
	static HierarchyPose* ForwardKinematics(HierarchyPose* p_world_out, HierarchyPose* p_obj_out, HierarchyPose* p_local, Hierarchy const* p_hierarchy,
		SpatialPose* parentActorPose);
	static HierarchyPose* PartialInverseKinematics(HierarchyPose* p_local_out, HierarchyPose* p_obj, Hierarchy const* hierarchy,
		SpatialPose* worldSpaceTarget, int32 startNode, int32 endNode, HierarchyPose* p_base);

	static HierarchyPose* Convert(HierarchyPose* p_out);
	static HierarchyPose* Concat(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1);

	static HierarchyPose* Triangular(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2, float u1, float u2);
	static HierarchyPose* BiNearest(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2, HierarchyPose* p3, float u0, float u1, float u);
	static HierarchyPose* BiLinear(HierarchyPose* p_out, HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2, HierarchyPose* p3, float u0, float u1, float u);
	static HierarchyPose* BiCubic(HierarchyPose* p_out,
		HierarchyPose* p0, HierarchyPose* p1, HierarchyPose* p2, HierarchyPose* p3,
		HierarchyPose* p4, HierarchyPose* p5, HierarchyPose* p6, HierarchyPose* p7,
		HierarchyPose* p8, HierarchyPose* p9, HierarchyPose* p10, HierarchyPose* p11,
		HierarchyPose* p12, HierarchyPose* p13, HierarchyPose* p14, HierarchyPose* p15,
		float u0, float u1, float u2, float u3, float u);
	
public:

	void Reset(const uint32 nodeCount);

	// copy full hierarchy pose
	//void a3hierarchyPoseCopy(const HierarchyPose* pose_out, const HierarchyPose* pose_in, const uint32 nodeCount);
public:
	TArray<SpatialPose*> Pose;
};
