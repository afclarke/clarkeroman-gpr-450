// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HierarchyNode.h"

/**
 *
 */
class GPR450UE4_API Hierarchy
{
public:
	Hierarchy(int32 nodeCount);
	~Hierarchy();
public:
	int32 SetNode(const uint32 index, const int32 parentIndex, const FString& name);
	int32 GetNodeIndexFromName(const FString& name) const;
	const TArray<FString> GetNodeNames();

	bool IsParentNode(const uint32 parentIndex, const uint32 otherIndex) const;
	bool IsChildNode(const uint32 childIndex, const uint32 otherIndex) const;
	bool IsSiblingNode(const uint32 siblingIndex, const uint32 otherIndex) const;
	bool IsAncestorNode(const uint32 ancestorIndex, const uint32 otherIndex) const;
	bool IsDescendantNode(const uint32 descendantIndex, const uint32 otherIndex) const;

	//void SaveBinary(const a3_FileStream* fileStream);
	//void LoadBinary(const a3_FileStream* fileStream);
	void CopyToString(uint8* str);
	void CopyFromString(const uint8* str);
	void GetStringSize();
	void Release();
public:
	TArray<HierarchyNode*> Nodes;
};
