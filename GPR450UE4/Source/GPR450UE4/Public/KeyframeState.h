//

#pragma once

#include "CoreMinimal.h"
#include "Keyframe.h"

#include "KeyframeState.generated.h"

USTRUCT()
struct FKeyframeState
{
	GENERATED_BODY()

public:
	int32 KeyframePIndex;
	int32 Keyframe0Index;
	int32 Keyframe1Index;
	int32 KeyframeNIndex;
	float KeyframeDuration;
	float KeyframeDurationInv;
	float KeyframeTime;
	float KeyframeParameter;
};
