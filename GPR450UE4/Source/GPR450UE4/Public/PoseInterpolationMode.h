//

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EPoseInterpolationMode : uint8
{
	Step,
	Nearest,
	Linear,
	Smoothstep,
	Smootherstep,
	Cubic,
	Triangular,
	BiNearest,
	BiLinear,
	BiCubic
};
