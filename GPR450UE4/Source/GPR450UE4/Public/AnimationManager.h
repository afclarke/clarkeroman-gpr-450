// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HierarchyState.h"

#include "AnimationManager.generated.h"

class ClipController;
class ClipPool;
class KeyframePool;
class AAnimationManager;
class UDataTable;


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GPR450UE4_API AAnimationManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAnimationManager();

public:
	//UFUNCTION(BlueprintCallable)
	//void HierarchySetPose(int32 poseIndex);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
private:
	UPROPERTY(EditAnywhere)
	UDataTable* AnimationData;

	//UPROPERTY(EditAnywhere)
	//TSubclassOf<AActor> ClassToFind;

	//UPROPERTY(EditAnywhere)
	//UClass* JointActorClass;

	//KeyframePool* KeyframePoolInstance;

	//ClipPool* ClipPoolInstance;

	//Hierarchy* HierarchyPtr;
	//HierarchyPoseGroup* pHierarchyPoseGroup;
	//HierarchyState* HierarchyStatePtr;
};
