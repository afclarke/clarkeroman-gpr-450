// Copyright Epic Games, Inc. All Rights Reserved.

#include "GPR450UE4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GPR450UE4, "GPR450UE4" );
